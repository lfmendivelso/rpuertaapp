package co.gov.policia.rpuerta;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.utilidad.SpinnerUtilidad;

public class DireccionCiudadanoActivity extends AppCompatActivity  implements LocationListener, MapEventsReceiver {


    // Constantes
    public static final long REFRESH_TIME = 500;
    public static final float REFRESH_DISTANCE = 2;

    // Campos
    private Ciudadano ciudadano;
    private Spinner spinnerDireccionOrigen;
    private EditText editTextDireccionOrigen;
    private EditText editTextDireccionNumero1;
    private EditText editTextDireccionNumero2;
    private Spinner spinnerDireccionSector;
    private EditText editTextDireccionDetalle;

    //Validador
    private AwesomeValidation validador;

    //Mapa
    private MapView direccionMapa;
    private IMapController controladorMapa;
    private Double latitud;
    private Double longitud;
    private LocationManager localizador;
    private GeoPoint localizacionActual;
    private ItemizedIconOverlay<OverlayItem> marcador;
    private MapEventsOverlay eventosDelMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direccion_ciudadano);
        ciudadano= null;
        String ciudadanoId = getIntent().getStringExtra("ciudadanoId");
        ciudadano = Ciudadano.findById(Ciudadano.class,Long.parseLong(ciudadanoId));
        System.out.println(ciudadano);

        spinnerDireccionOrigen = (Spinner) findViewById(R.id.sp_direccion_origen);
        editTextDireccionOrigen = (EditText) findViewById(R.id.etx_direccion_origen);
        editTextDireccionNumero1 = (EditText) findViewById(R.id.etx_direccion_numero1);
        editTextDireccionNumero2 = (EditText) findViewById(R.id.etx_direccion_numero2);
        spinnerDireccionSector = (Spinner) findViewById(R.id.sp_direccion_sector);
        editTextDireccionDetalle = (EditText) findViewById(R.id.etx_direccion_detalle);
        direccionMapa = (MapView) findViewById(R.id.mp_direccion_ciudadano);

        configurarValidador();
        cargarOpcionesDeRespuesta();
        ajustarLocalizador();
        configurarMapa();
    }

    // Logica de la Actividad
    private Boolean validarDatos(){
        Boolean validado = validador.validate();
        if(!SpinnerUtilidad.validarSpinner(spinnerDireccionOrigen)){
            validado = false;
        }
        return validado;
    }

    private void configurarValidador(){
        validador = new AwesomeValidation(ValidationStyle.BASIC);
        validador.addValidation(this,R.id.etx_direccion_origen, RegexTemplate.NOT_EMPTY,R.string.error_direccion_origen);
        validador.addValidation(this,R.id.etx_direccion_numero1, RegexTemplate.NOT_EMPTY,R.string.error_direccion_numero1);
        validador.addValidation(this,R.id.etx_direccion_numero2, RegexTemplate.NOT_EMPTY,R.string.error_direccion_numero2);
    }

    private void cargarOpcionesDeRespuesta(){
        // Indicaciones
        ArrayAdapter<CharSequence> indicaciones = ArrayAdapter.createFromResource(this, R.array.txlist_indicaciones, android.R.layout.simple_spinner_item);
        indicaciones.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDireccionOrigen.setAdapter(indicaciones);

        // Sector
        ArrayAdapter<CharSequence> sectores = ArrayAdapter.createFromResource(this, R.array.txlist_sector, android.R.layout.simple_spinner_item);
        sectores.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDireccionSector.setAdapter(sectores);
    }

    public void direccionContinuar(View view){
        if(validarDatos()){
            String origen = editTextDireccionOrigen.getText().toString();
            String numero1 = editTextDireccionNumero1.getText().toString();
            String numero2 = editTextDireccionNumero2.getText().toString();
            String detalle = editTextDireccionDetalle.getText().toString();
            String sector = spinnerDireccionSector.getSelectedItem().toString();
            String direccion = origen+" # "+numero1+" - "+numero2;
            if(!sector.equals(SpinnerUtilidad.SPINNER_ENTRADA)){
                direccion = direccion+" "+sector;
            }
            direccion = direccion+". "+detalle;
            ciudadano.setDireccion(direccion);
            ciudadano.setLatitud(latitud);
            ciudadano.setLongitud(longitud);
            ciudadano.save();

            Intent seccion = new Intent(this,SeccionActivity.class);
            seccion.putExtra("ciudadanoId",ciudadano.getId().toString());
            seccion.putExtra("seccion",1+"");
            startActivity(seccion);
            finish();
        }
        else{
            Toast.makeText(this,"Verifique todos los campos",Toast.LENGTH_SHORT).show();
        }
    }

    public void direccionCancelar(View view){
        finish();
    }

    // Operaciones del Mapa
    public void ajustarLocalizador() {
        localizador = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        localizador.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, REFRESH_TIME, REFRESH_DISTANCE, this);
    }

    public void centrarMapa() {
        localizacionActual = new GeoPoint(latitud, longitud);
        controladorMapa.setCenter(localizacionActual);
    }

    public void cambiarMarcadorALocalizacionActual() {
        ArrayList<OverlayItem> itemsDelMarcador = new ArrayList<>();
        OverlayItem currentLocationMarker = new OverlayItem("Localizacion actual","",localizacionActual);
        itemsDelMarcador.add(currentLocationMarker);
        marcador = new ItemizedIconOverlay<>(this, itemsDelMarcador, null);
        direccionMapa.getOverlays().clear();
        direccionMapa.getOverlays().add(0,eventosDelMapa);
        direccionMapa.getOverlays().add(marcador);
    }

    private void configurarMapa(){
        direccionMapa.setTileSource(TileSourceFactory.MAPNIK);
        direccionMapa.getTileProvider().clearTileCache();
        controladorMapa = direccionMapa.getController();
        controladorMapa.setZoom(16);
        direccionMapa.setBuiltInZoomControls(true);
        direccionMapa.setMultiTouchControls(true);
        eventosDelMapa = new MapEventsOverlay(this, this);
    }

    // Localizacion Actual
    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        localizador = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        localizador.requestLocationUpdates(LocationManager.GPS_PROVIDER, REFRESH_TIME, REFRESH_DISTANCE, this);

    }

    // Eventos del Mapa
    @Override
    public boolean singleTapConfirmedHelper(GeoPoint geoPoint) {
        latitud = geoPoint.getLatitude();
        longitud = geoPoint.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint geoPoint) {
        return false;
    }
}
