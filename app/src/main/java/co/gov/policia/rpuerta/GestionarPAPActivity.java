package co.gov.policia.rpuerta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.List;
import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.modelo.ReportePAP;

public class GestionarPAPActivity extends AppCompatActivity {

    private TextView conteo;
    private String reporteId;
    private ReportePAP reporteActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar_pap);
        reporteId = getIntent().getStringExtra("reporteId");
        reporteActual = ReportePAP.findById(ReportePAP.class,Long.parseLong(reporteId));
        this.conteo = (TextView) findViewById(R.id.lb_gestionarpap_conteo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Ciudadano> ciudadanos = Ciudadano.find(Ciudadano.class,"reporte=?",reporteActual.getId().toString());
        for(Ciudadano c : ciudadanos){
            System.out.println(c.toString());
        }
        String[] texto = conteo.getText().toString().split(":");
        conteo.setText(texto[0]+": "+ciudadanos.size());
    }

    public void gestionarPAPNuevoCiudadano(View view){
        Intent nuevoCiudadano = new Intent(this, RegistrarCiudadanoActivity.class);
        nuevoCiudadano.putExtra("reporteId",reporteId);
        startActivity(nuevoCiudadano);
    }

    public void gestionarPAPFinalizar(View view){

        // Guardar

        // Enviar

        // Cerrar vista
        finish();
    }

    public void gestionarPAPCancelar(View view){
        // Eliminar

        // Quitar actividad
        finish();
    }
}
