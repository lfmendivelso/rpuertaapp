package co.gov.policia.rpuerta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import co.gov.policia.rpuerta.logica.bd.Configurador;

public class InicialActivity extends AppCompatActivity {

    private Button btnInicialContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicial);
        btnInicialContinuar = (Button) findViewById(R.id.btn_inicial_continuar);
        btnInicialContinuar.setEnabled(false);
        new Configurador(this).execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        btnInicialContinuar.setEnabled(true);
    }

    public void continuarInicio(View view){
        Intent iniciarSesion = new Intent(this, IniciarSesionActivity.class);
        startActivity(iniciarSesion);
        finish();
    }
}
