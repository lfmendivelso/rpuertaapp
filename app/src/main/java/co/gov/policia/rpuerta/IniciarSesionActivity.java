package co.gov.policia.rpuerta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class IniciarSesionActivity extends AppCompatActivity {

    public static final String USUARIO_PRUEBA = "patrullero.1";
    public static final String CLAVE_PRUEBA = "policia";

    private TextView error;
    private EditText usuario;
    private EditText clave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
        error = (TextView) findViewById(R.id.tx_iniciarsesion_error);
        usuario = (EditText) findViewById(R.id.etx_iniciarsesion_usuario);
        clave = (EditText) findViewById(R.id.etx_iniciarsesion_clave);
    }

    public void cerrar(View view){
        finish();
    }

    public void iniciar(View view){

        String usuarioTx = usuario.getText().toString();
        String claveTx = clave.getText().toString();

        if(usuarioTx.length()>0 && claveTx.length()>0){
            if(usuarioTx.equals(USUARIO_PRUEBA) && claveTx.equals(CLAVE_PRUEBA)){
                Toast.makeText(this,"Ha iniciado sesion",Toast.LENGTH_SHORT).show();
                error.setText("");
                Intent menuPrincipal = new Intent(this, MenuPrincipalActivity.class);
                startActivity(menuPrincipal);
                finish();
            }
            else{
                error.setText(getString(R.string.msjerror_iniciarsesion_1));
                usuario.setText("");
                clave.setText("");
            }
        }
        else{
            error.setText(getString(R.string.msjerror_iniciarsesion_1));
            usuario.setText("");
            clave.setText("");
        }
    }
}
