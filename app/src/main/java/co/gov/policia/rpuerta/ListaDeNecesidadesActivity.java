package co.gov.policia.rpuerta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.modelo.Pregunta;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.modelo.Respuesta;
import co.gov.policia.rpuerta.modelo.Seccion;

public class ListaDeNecesidadesActivity extends AppCompatActivity {

    public static final Integer SECCION_ID=6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_necesidades);

        Cuadrante cuadrante = Cuadrante.findById(Cuadrante.class, Long.parseLong(getIntent().getStringExtra("cuadranteId")));
        ReportePAP reporte = ReportePAP.find(ReportePAP.class, "cuadrante=? and version=?", cuadrante.getId().toString(), getIntent().getStringExtra("version")).get(0);
        Seccion seccion = Seccion.findById(Seccion.class, SECCION_ID);

        List<String> respuestas = new ArrayList<>();
        List<Ciudadano> ciudadanos = Ciudadano.find(Ciudadano.class, "reporte=?", reporte.getId().toString());

        for (Pregunta pregunta : seccion.getPreguntas()) {
            for (Ciudadano ciudadano : ciudadanos) {
                List<Respuesta> consulta = Respuesta.find(Respuesta.class, "ciudadano=? and pregunta=?", ciudadano.getId().toString(), pregunta.getId().toString());
                if (!consulta.isEmpty()) {
                    Respuesta respuesta = consulta.get(0);
                    respuestas.add(respuesta.getRespuestaAbierta());
                }
                else{
                    System.out.println("Resultado no encontrado para "+ciudadano.getId());
                }
            }
        }

        System.out.println(respuestas);
        ArrayAdapter<String> datosDeLaLista = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, respuestas);
        ListView lista = (ListView) findViewById(R.id.lv_listanecesidades_lista);
        lista.setAdapter(datosDeLaLista);

    }

    public void listaNecesidadesRegresar(View view) {
        finish();
    }
}
