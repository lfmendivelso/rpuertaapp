package co.gov.policia.rpuerta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.utilidad.SpinnerUtilidad;

public class MenuEstadoDelCuadranteActivity extends AppCompatActivity {

    public static final Integer MIEDO=1;
    public static final Integer VICTIMIZACION=2;
    public static final Integer APOYO=3;
    public static final Integer EVALUACION=4;
    public static final Integer LISTA=5;

    private Cuadrante cuadrante;
    private List<ReportePAP> reportes;
    private Spinner spPeriodos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_estado_del_cuadrante);
        Long cuadranteId = Long.parseLong(getIntent().getStringExtra("cuadranteId"));
        cuadrante = Cuadrante.findById(Cuadrante.class,cuadranteId);
        reportes = ReportePAP.find(ReportePAP.class,"cuadrante=?",cuadranteId.toString());
        spPeriodos = (Spinner) findViewById(R.id.sp_menuestado_version);
        cargarPeriodos();
    }

    public void menuEstadoVerMapa(View view){
        if(SpinnerUtilidad.validarSpinner(spPeriodos)) {
            switch (view.getId()) {
                case R.id.btn_menuestado_miedo:
                    generarReporte(MIEDO);
                    break;
                case R.id.btn_menuestado_victimizacion:
                    generarReporte(VICTIMIZACION);
                    break;
                case R.id.btn_menuestado_apoyo:
                    generarReporte(APOYO);
                    break;
                case R.id.btn_menuestado_evaluacion:
                    generarReporte(EVALUACION);
                    break;
                default:
                    generarReporte(LISTA);
                    break;
            }
        }
    }

    private void cargarPeriodos(){
        ArrayList<String> periodosOpciones = SpinnerUtilidad.obtenerListaDeNombres(reportes);
        ArrayAdapter periodos = new ArrayAdapter(this, android.R.layout.simple_spinner_item, periodosOpciones);
        periodos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPeriodos.setAdapter(periodos);
    }

    private void generarReporte(Integer tipo){
        if(tipo!=LISTA){
            Intent reporte = new Intent(this,VerMapaDeTemaActivity.class);
            reporte.putExtra("tipo",""+tipo);
            reporte.putExtra("version",spPeriodos.getSelectedItem().toString());
            reporte.putExtra("cuadranteId",cuadrante.getId().toString());
            startActivity(reporte);
        }
        else{
            Intent lista = new Intent(this,ListaDeNecesidadesActivity.class);
            lista.putExtra("version",spPeriodos.getSelectedItem().toString());
            lista.putExtra("cuadranteId",cuadrante.getId().toString());
            startActivity(lista);
        }
    }

}
