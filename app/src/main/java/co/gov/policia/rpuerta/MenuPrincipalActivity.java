package co.gov.policia.rpuerta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MenuPrincipalActivity extends AppCompatActivity {

    public final static String NUEVO="Nuevo";
    public final static String ESTADO="Estado";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
    }

    public void menuNuevoPuertaAPuerta(View view){
        Intent selcuadrante = new Intent(this, SeleccionarCuadranteActivity.class);
        selcuadrante.putExtra("tipo",NUEVO);
        startActivity(selcuadrante);
    }

    public void menuVerEstado(View view){

        Intent selcuadrante = new Intent(this, SeleccionarCuadranteActivity.class);
        selcuadrante.putExtra("tipo",ESTADO);
        startActivity(selcuadrante);
    }

    public void menuSalir(View view){
        finish();
    }

}
