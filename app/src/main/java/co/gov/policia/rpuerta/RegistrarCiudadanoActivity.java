package co.gov.policia.rpuerta;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.modelo.RelacionConVecinos;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.modelo.TiempoDeResidencia;
import co.gov.policia.rpuerta.modelo.TipoDeDocumento;
import co.gov.policia.rpuerta.modelo.TipoDeVivienda;
import co.gov.policia.rpuerta.utilidad.SpinnerUtilidad;

public class RegistrarCiudadanoActivity extends AppCompatActivity{

    // Campos
    private Spinner spinnerTipoDocumento;
    private EditText editTextDocumento;
    private EditText editTextCorreo;
    private Spinner spinnerPropietario;
    private Spinner spinnerTiempoVivienda;
    private Spinner spinnerRelacionVecinos;
    private Spinner spinnerTipoVivienda;
    private String reporteId;
    private String documento;
    private ReportePAP reporte;

    //Validacion
    private AwesomeValidation validador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_ciudadano);
        reporteId = getIntent().getStringExtra("reporteId");
        reporte = ReportePAP.findById(ReportePAP.class,Long.parseLong(reporteId));

        // Leer elementos de la vista
        spinnerTipoDocumento = (Spinner) findViewById(R.id.spn_ciudadano_tipodoc);
        editTextDocumento = (EditText) findViewById(R.id.etx_ciudadano_documento);
        editTextCorreo = (EditText) findViewById(R.id.etx_ciudadano_correo);
        spinnerPropietario = (Spinner) findViewById(R.id.spn_ciudadano_propietario);
        spinnerTiempoVivienda = (Spinner) findViewById(R.id.spn_ciudadano_tiempoResidencia);
        spinnerRelacionVecinos = (Spinner) findViewById(R.id.spn_ciudadano_relacion);
        spinnerTipoVivienda = (Spinner) findViewById(R.id.spn_ciudadano_tipovivienda);
        cargarOpcionesDeRespuesta();
        configurarValidador();

    }

    //Operaciones
    public void registrarCiudadano(View view){
        if(validarDatos()){
            documento="";
            documento = editTextDocumento.getText().toString();
            List<Ciudadano> previos = Ciudadano.find(Ciudadano.class,"documento=? and reporte=?",documento,reporteId);
            if(previos.size()>0){
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Ciudadano previamente registrado")
                        .setMessage("Este documento ya ha sido registrado previamente, ¿Desea actualizar la información del mismo?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                crearNuevoCiudadano(true);
                            }

                        })
                        .setNegativeButton("NO", null)
                        .show();
            }
            else{
                crearNuevoCiudadano(false);
            }
        }
        else{
            Toast.makeText(this,"Verifique todos los campos",Toast.LENGTH_SHORT).show();
        }
    }

    private void crearNuevoCiudadano(Boolean actualizar){

        String propietario = spinnerPropietario.getSelectedItem().toString();

        TipoDeDocumento tipoDeDocumento = TipoDeDocumento.find(TipoDeDocumento.class,"nombre=?",spinnerTipoDocumento.getSelectedItem().toString()).get(0);

        TiempoDeResidencia tiempoDeResidencia = TiempoDeResidencia.find(TiempoDeResidencia.class,"tiempo=?",spinnerTiempoVivienda.getSelectedItem().toString()).get(0);

        TipoDeVivienda tipoDeVivienda = TipoDeVivienda.find(TipoDeVivienda.class,"nombre=?",spinnerTipoVivienda.getSelectedItem().toString()).get(0);

        RelacionConVecinos relacionConVecinos = RelacionConVecinos.find(RelacionConVecinos.class,"relacion=?",spinnerRelacionVecinos.getSelectedItem().toString()).get(0);

        String correo = editTextCorreo.getText().toString();

        Ciudadano ciudadano=new Ciudadano();
        ciudadano.setDocumento(documento);
        ciudadano.setCorreo(correo);
        ciudadano.setPropietario("SI".equals(propietario));
        ciudadano.setTipoDeDocumento(tipoDeDocumento);
        ciudadano.setTiempoDeResidencia(tiempoDeResidencia);
        ciudadano.setTipoDeVivienda(tipoDeVivienda);
        ciudadano.setRelacionConVecinos(relacionConVecinos);
        ciudadano.setReporte(reporte);
        System.out.println(ciudadano);

        if(actualizar){
            Ciudadano temp = Ciudadano.find(Ciudadano.class,"documento=? and reporte=?",documento,reporteId).get(0);
            ciudadano.setId(temp.getId());
            ciudadano.save();
            System.out.println("Actualiza");
        }
        else{
            ciudadano.save();
            System.out.println("Nuevo");
        }

        Intent direccionCiudadano = new Intent(this, DireccionCiudadanoActivity.class);
        direccionCiudadano.putExtra("ciudadanoId",ciudadano.getId().toString());
        startActivity(direccionCiudadano);

    }

    public void cancelarRegistro(View view){
        validador.clear();
        finish();
    }

    private Boolean validarDatos(){
        Boolean validado = validador.validate();
        System.out.println("Campos: "+validado);
        if(!SpinnerUtilidad.validarSpinner(spinnerTipoDocumento)){
            validado = false;
        }
        if(!SpinnerUtilidad.validarSpinner(spinnerPropietario)){
            validado = false;
        }
        if(!SpinnerUtilidad.validarSpinner(spinnerTipoVivienda)){
            validado = false;
        }
        if(!SpinnerUtilidad.validarSpinner(spinnerTiempoVivienda)){
            validado = false;
        }
        if(!SpinnerUtilidad.validarSpinner(spinnerRelacionVecinos)){
            validado = false;
        }
        System.out.println("Spinner: "+validado);
        return validado;
    }

    private void configurarValidador(){
        validador = new AwesomeValidation(ValidationStyle.BASIC);
        validador.addValidation(this,R.id.etx_ciudadano_documento, RegexTemplate.NOT_EMPTY,R.string.error_ciudadano_documento);
        validador.addValidation(this,R.id.etx_ciudadano_correo, RegexTemplate.NOT_EMPTY,R.string.error_ciudadano_email_vacio);
        //validador.addValidation(this,R.id.etx_ciudadano_correo, Patterns.EMAIL_ADDRESS,R.string.error_ciudadano_email_formato);
    }

    private void cargarOpcionesDeRespuesta(){
        // Tipos de Documentos
        ArrayList<String> tiposDocumentos = SpinnerUtilidad.obtenerListaDeNombres(TipoDeDocumento.listAll(TipoDeDocumento.class));
        ArrayAdapter tipoDocumentosAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tiposDocumentos);
        tipoDocumentosAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoDocumento.setAdapter(tipoDocumentosAdapter);

        // Tiempo de Vivienda
        ArrayList<String> tiempos = SpinnerUtilidad.obtenerListaDeNombres(TiempoDeResidencia.listAll(TiempoDeResidencia.class));
        ArrayAdapter tiemposAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tiempos);
        tiemposAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTiempoVivienda.setAdapter(tiemposAdapter);

        // Relación
        ArrayList<String> relaciones = SpinnerUtilidad.obtenerListaDeNombres(RelacionConVecinos.listAll(RelacionConVecinos.class));
        ArrayAdapter relacionAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, relaciones );
        relacionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRelacionVecinos.setAdapter(relacionAdapter);

        // Propietario
        ArrayList<String> propietario = new ArrayList<>();
        propietario.add(SpinnerUtilidad.SPINNER_ENTRADA);
        propietario.add("SI");
        propietario.add("NO");
        ArrayAdapter propietarioAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, propietario );
        propietarioAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPropietario.setAdapter(propietarioAdapter);

        // Tipo de Vivienda
        ArrayList<String> tiposVivienda = SpinnerUtilidad.obtenerListaDeNombres(TipoDeVivienda.listAll(TipoDeVivienda.class));
        ArrayAdapter tiposViviendaAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tiposVivienda);
        tiposViviendaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoVivienda.setAdapter(tiposViviendaAdapter);
    }

}
