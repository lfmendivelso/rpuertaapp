package co.gov.policia.rpuerta;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.logica.RespuestaTipoMapa;
import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.modelo.OpcionDeRespuesta;
import co.gov.policia.rpuerta.modelo.Pregunta;
import co.gov.policia.rpuerta.modelo.Respuesta;
import co.gov.policia.rpuerta.modelo.Seccion;


public class SeccionActivity extends AppCompatActivity {

    private LinearLayout formulario;
    private TextView titulo;
    private TextView descripcion;
    private Ciudadano ciudadano;
    private Seccion seccion;
    private Long seccionId;
    private List<TextView> textos;
    private List<View> campos;
    private List<RespuestaTipoMapa> mapas;

    private Button continuar;
    private String reporteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seccion);
        textos = new ArrayList<>();
        campos = new ArrayList<>();
        mapas = new ArrayList<>();
        String ciudadanoId = getIntent().getStringExtra("ciudadanoId");
        ciudadano = Ciudadano.findById(Ciudadano.class,Long.parseLong(ciudadanoId));
        seccionId = Long.parseLong(getIntent().getStringExtra("seccion"));
        seccion = Seccion.findById(Seccion.class, seccionId);

        formulario = (LinearLayout) findViewById(R.id.lnly_seccion_preguntas);

        titulo = (TextView) findViewById(R.id.lb_seccion_titulo);
        descripcion = (TextView) findViewById(R.id.lb_seccion_descripcion);

        titulo.setText(seccion.getNombre());
        descripcion.setText(seccion.getDescripcion());

        for (Pregunta p : seccion.getPreguntas()) {
            generarPregunta(p);
        }
        generarBotones();
    }

    // Acciones de la Actividad
    private void continuar() {
        if(validarCampos()){
            guardarRepuestas();
            Intent seguienteSeccion = new Intent(this, SeccionActivity.class);
            seguienteSeccion.putExtra("ciudadanoId", ciudadano.getId().toString());
            seguienteSeccion.putExtra("seccion", (seccionId + 1) + "");
            startActivity(seguienteSeccion);
        }
    }

    private void finalizar() {
        if(validarCampos()) {
            guardarRepuestas();
            regresarAlMenuDeGestion();
        }
    }

    public void regresarAlMenuDeGestion(){
        Intent gestionarPAP = new Intent(getApplicationContext(), GestionarPAPActivity.class);
        gestionarPAP.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        gestionarPAP.putExtra("reporteId",ciudadano.getReporte().getId().toString());
        startActivity(gestionarPAP);
    }

    private void guardarRepuestas() {
        for(int i=0;i<seccion.getPreguntas().size();i++){
            Pregunta pregunta = seccion.getPreguntas().get(i);
            List<Respuesta> previa = Respuesta.find(Respuesta.class,"ciudadano=? and pregunta=?",ciudadano.getId().toString(),pregunta.getId().toString());
            Respuesta respuesta;
            if(previa.size()>0){
                respuesta = previa.get(0);
            }
            else{
                respuesta = new Respuesta();
                respuesta.setCiudadano(ciudadano);
                respuesta.setLatitud(ciudadano.getLatitud());
                respuesta.setLongitud(ciudadano.getLongitud());
                respuesta.setPregunta(pregunta);
            }

            if(campos.get(i) instanceof TextView){
                TextView respuestaEnTexto = (TextView) campos.get(i);
                respuesta.setRespuestaAbierta(respuestaEnTexto.getText().toString());
                respuesta.save();
            }
            else if(campos.get(i) instanceof MapView){
                RespuestaTipoMapa mapa = mapas.get(i);
                respuesta.setLatitud(mapa.getLatitud());
                respuesta.setLongitud(mapa.getLongitud());
                respuesta.setRespuestaAbierta(mapa.getReferencia().getText().toString());
                respuesta.save();
            }
            else{
                RadioGroup grupo = (RadioGroup) campos.get(i);
                int op = grupo.getCheckedRadioButtonId();
                RadioButton opcionRB = (RadioButton) findViewById(op);
                OpcionDeRespuesta opcionDeRespuesta = OpcionDeRespuesta.find(OpcionDeRespuesta.class,"opcion=? and tipo=?",opcionRB.getText().toString(),pregunta.getTipoDePregunta().getId().toString()).get(0);
                respuesta.setRespuestaPorOpcion(opcionDeRespuesta);
                respuesta.save();
            }
        }
    }

    private void borrarCiudadano() {
        List<Respuesta> respuestas = Respuesta.find(Respuesta.class,"ciudadano=?",ciudadano.getId().toString());
        for(Respuesta respuesta : respuestas){
            respuesta.delete();
        }
        ciudadano.delete();
        regresarAlMenuDeGestion();
    }

    private void cancelarRegistro() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Advertencia")
                .setMessage("¿Desea cancelar el registro del ciudadano? (Esto implicar borrar toda información registrada del ciudadano en el presente puerta a puerta).")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        borrarCiudadano();
                        finish();
                    }

                })
                .setNegativeButton("NO", null)
                .show();
    }


    // Generación de Preguntas
    private void generarPregunta(Pregunta pregunta) {

        //TextView
        TextView txPregunta = new TextView(this);
        txPregunta.setText(pregunta.getPregunta());
        txPregunta.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        txPregunta.setTextAppearance(this, android.R.style.MediaButton);
        formulario.addView(txPregunta);
        textos.add(txPregunta);
        System.out.println(pregunta.getTipoDePregunta().getId().intValue());
        switch (pregunta.getTipoDePregunta().getId().intValue()) {
            case 1:
                generarCampo(pregunta);
                break;
            case 8:
                generarMapa(pregunta);
                break;
            default:
                generarRadioGroup(pregunta);
                break;
        }

    }

    private void generarRadioGroup(Pregunta pregunta) {
        // Campo
        RadioGroup opciones = new RadioGroup(this);
        opciones.setOrientation(RadioGroup.VERTICAL);
        opciones.setLayoutParams(new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        for (OpcionDeRespuesta opcionDeRespuesta : pregunta.getTipoDePregunta().getOpciones()) {
            RadioButton opcion = new RadioButton(this);
            opcion.setText(opcionDeRespuesta.getOpcion());
            opciones.addView(opcion);
        }
        formulario.addView(opciones);
        campos.add(opciones);
        mapas.add(null);
    }

    private void generarCampo(Pregunta pregunta) {
        EditText editText = new EditText(this);
        editText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        formulario.addView(editText);
        campos.add(editText);
        mapas.add(null);
    }

    private void generarMapa(Pregunta pregunta) {
        RespuestaTipoMapa tipoMapa = new RespuestaTipoMapa(this,ciudadano.getLatitud(),ciudadano.getLongitud());
        formulario.addView(tipoMapa.getReferencia());
        formulario.addView(tipoMapa.getMapa());
        campos.add(tipoMapa.getMapa());
        mapas.add(tipoMapa);
    }


    //Configuración adicional
    private void generarBotones() {
        Button cancelar = new Button(this);
        cancelar.setText("Cancelar");
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarRegistro();
            }
        });

        if (seccionId != Seccion.count(Seccion.class)) {
            continuar = new Button(this);
            continuar.setText("Continuar");
            continuar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    continuar();
                }
            });
        } else {
            continuar = new Button(this);
            continuar.setText("Finalizar");
            continuar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalizar();
                }
            });
        }
        continuar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        continuar.setTextColor(Color.WHITE);
        formulario.addView(continuar);
        formulario.addView(cancelar);
    }

    private Boolean validarCampos(){
        Boolean valido = true;
        for(int i=0;i<campos.size();i++){
            if(campos.get(i) instanceof TextView){
                TextView campo = (TextView) campos.get(i);
                if(campo.getText().toString().equals("")){
                    campo.setError("Debe ingresar una respuesta a la pregunta. Sí no la hay, ingrese 'NA' como respuesta");
                    valido = false;
                }
            }
            else if(campos.get(i) instanceof MapView){
                RespuestaTipoMapa mapa = mapas.get(i);
                if(mapa.getLatitud()==ciudadano.getLatitud() && mapa.getLongitud()==ciudadano.getLongitud()){
                    String mensaje = "La posición en el mapa es igual a la ubicación del ciudadano.";
                    Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show();
                    mapa.getReferencia().setError(mensaje);
                    valido = false;
                }
            }
            else{
                RadioGroup campo = (RadioGroup) campos.get(i);
                if(campo.getCheckedRadioButtonId()<0){
                    valido = false;
                    Toast.makeText(this,"No ha llenado todos los campos, por favor verifique las respuestas",Toast.LENGTH_SHORT).show();
                }
            }
        }
        return valido;
    }

}
