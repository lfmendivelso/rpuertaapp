package co.gov.policia.rpuerta;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.views.MapView;
import java.util.Date;
import java.util.List;
import co.gov.policia.rpuerta.logica.BuscadorDeCuadrante;
import co.gov.policia.rpuerta.logica.MapaGPS;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.modelo.ReportePAP;

public class SeleccionarCuadranteActivity extends AppCompatActivity {

    private MapaGPS mapa;
    private TextView referencia;
    private Cuadrante cuadrante;
    private BuscadorDeCuadrante buscadorDeCuadrante;
    private String destino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_cuadrante);
        destino = getIntent().getStringExtra("tipo");
        referencia = (TextView) findViewById(R.id.lb_selcuadrante_informacion);
        mapa = new MapaGPS(this, (MapView) findViewById(R.id.mp_selcuadrante_mapa));
        cuadrante = null;
    }

    public void scContinuar(View view) {
        if(buscadorDeCuadrante!=null) {
            cuadrante = buscadorDeCuadrante.getCuadrante();
            if (cuadrante != null) {
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Confirmacion")
                        .setMessage("¿Es el cuadrante de su interes?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(MenuPrincipalActivity.ESTADO.equals(destino)){
                                    verEstadoDeCuadrante();
                                }
                                else{
                                    generarPuertaAPuerta(new Date());
                                }

                            }

                        })
                        .setNegativeButton("NO", null)
                        .show();
            }
        }
        else{
            Toast.makeText(this,"Debe pulsar el boton buscar antes de continuar, para encontrar el cuadrante asociado a su posición",Toast.LENGTH_SHORT).show();
        }
    }

    public void seleccionarCuadranteBuscar(View view) {
        Double latitud = mapa.getLatitud();
        Double longitud = mapa.getLongitud();
        cuadrante = null;
        buscadorDeCuadrante = new BuscadorDeCuadrante(this,cuadrante,referencia,latitud,longitud,mapa);
        buscadorDeCuadrante.execute();
    }

    public void scCancelar(View view) {
        finish();
    }

    private void generarPuertaAPuerta(Date fechaActual) {
        String cuadranteId=cuadrante.getId().toString();
        String version = (fechaActual.getYear() + 1900) + "-" + (fechaActual.getMonth()+1);
        List<ReportePAP> previo = ReportePAP.find(ReportePAP.class,"cuadrante=? and version=?",cuadranteId,version);
        ReportePAP reporte;
        if(previo.size()==0){
            reporte = new ReportePAP();
            reporte.setVersion(version);
            reporte.setCuadrante(cuadrante);
            reporte.save();
            Toast.makeText(this,"Se ha creado un nuevo Puerta a Puerta",Toast.LENGTH_SHORT).show();
        }
        else{
            reporte = previo.get(0);
            int annoActual = fechaActual.getYear() + 1900;
            int mesActual = fechaActual.getMonth();
            int annoReporte = Integer.parseInt(reporte.getVersion().split("-")[0]);
            int mesReporte = Integer.parseInt(reporte.getVersion().split("-")[1]);
            if(annoActual>annoReporte || mesActual>mesReporte){
                reporte = new ReportePAP();
                reporte.setVersion(version);
                reporte.setCuadrante(cuadrante);
                reporte.save();
                Toast.makeText(this,"Se ha creado un nuevo Puerta a Puerta",Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this,"Se ha retomado un Puerta a Puerta del presente periodo",Toast.LENGTH_SHORT).show();
            }
        }
        Intent gestionar = new Intent(this, GestionarPAPActivity.class);
        gestionar.putExtra("reporteId",reporte.getId().toString());
        startActivity(gestionar);
        finish();
    }

    public void verEstadoDeCuadrante(){
        Intent estadoDeCuadrante = new Intent(this,MenuEstadoDelCuadranteActivity.class);
        estadoDeCuadrante.putExtra("cuadranteId",cuadrante.getId().toString());
        startActivity(estadoDeCuadrante);
        finish();
    }

    public void seleccionarCuadranteManual(View view) {
        Intent manual = new Intent(this, SeleccionarCuadranteManualActivity.class);
        manual.putExtra("tipo",destino);
        startActivity(manual);
        finish();
    }
}
