package co.gov.policia.rpuerta;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import org.osmdroid.views.MapView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import co.gov.policia.rpuerta.logica.BuscadorDeCuadrante;
import co.gov.policia.rpuerta.logica.MapaManual;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.modelo.Region;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.utilidad.SpinnerUtilidad;

public class SeleccionarCuadranteManualActivity extends AppCompatActivity {

    private MapView mapaView;
    private MapaManual mapa;
    private TextView referencia;
    private Cuadrante cuadrante;
    private ComandoDePolicia comando;
    private Spinner spRegiones;
    private Spinner spComandos;
    private BuscadorDeCuadrante buscadorDeCuadrante;
    private String destino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_cuadrante_manual);
        destino = getIntent().getStringExtra("tipo");
        referencia = (TextView) findViewById(R.id.tx_selcuadrante_cuadrante);
        mapaView = (MapView) findViewById(R.id.mp_selcuadrantemanual_mapa);
        mapa = new MapaManual(this,mapaView,4.0866629,-81.9760513);
        spRegiones = (Spinner) findViewById(R.id.sp_selcuadrante_region);
        spComandos= (Spinner) findViewById(R.id.sp_selcuadrante_comando);
        configurarSpinnerDeRegiones();
    }

    public void seleccionarCuadranteManualContinuar(View view) {
        cuadrante = buscadorDeCuadrante.getCuadrante();
        if(cuadrante != null){
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Confirmacion")
                    .setMessage("¿Desea iniciar el Puerta a Puerta?")
                    .setPositiveButton("SI", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(MenuPrincipalActivity.ESTADO.equals(destino)){
                                verEstadoDeCuadrante();
                            }
                            else{
                                generarPuertaAPuerta(new Date());
                            }
                        }

                    })
                    .setNegativeButton("NO", null)
                    .show();
        }
    }

    public void seleccionarCuadranteManualCancelar(View view) {
        finish();
    }

    private void generarPuertaAPuerta(Date fechaActual) {
        String cuadranteId=cuadrante.getId().toString();
        String version = (fechaActual.getYear() + 1900) + "-" + (fechaActual.getMonth()+1);
        List<ReportePAP> previo = ReportePAP.find(ReportePAP.class,"cuadrante=? and version=?",cuadranteId,version);
        ReportePAP reporte;
        if(previo.size()==0){
            reporte = new ReportePAP();
            reporte.setVersion(version);
            reporte.setCuadrante(cuadrante);
            reporte.save();
            Toast.makeText(this,"Se ha creado un nuevo Puerta a Puerta",Toast.LENGTH_SHORT).show();
        }
        else{
            reporte = previo.get(0);
            System.out.println("Previo");
            int annoActual = fechaActual.getYear() + 1900;
            int mesActual = fechaActual.getMonth();
            int annoReporte = Integer.parseInt(reporte.getVersion().split("-")[0]);
            int mesReporte = Integer.parseInt(reporte.getVersion().split("-")[1]);
            if(annoActual>annoReporte || mesActual>mesReporte){
                reporte = new ReportePAP();
                reporte.setVersion(version);
                reporte.setCuadrante(cuadrante);
                reporte.save();
                Toast.makeText(this,"Se ha creado un nuevo Puerta a Puerta",Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this,"Se ha retomado un Puerta a Puerta del presente periodo",Toast.LENGTH_SHORT).show();
            }
        }
        Intent gestionar = new Intent(this, GestionarPAPActivity.class);
        gestionar.putExtra("reporteId",reporte.getId().toString());
        startActivity(gestionar);
        finish();
    }

    public void seleccionarCuadranteManualBuscar(View view) {
        Double latitud = mapa.getLatitud();
        Double longitud = mapa.getLongitud();
        cuadrante = null;
        buscadorDeCuadrante = new BuscadorDeCuadrante(this,cuadrante,referencia,latitud,longitud,mapa,comando);
        buscadorDeCuadrante.execute();
    }

    private void configurarSpinnerDeRegiones(){
        ArrayList<String> regiones = SpinnerUtilidad.obtenerListaDeNombres(Region.listAll(Region.class));
        ArrayAdapter regionesAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, regiones);
        regionesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRegiones.setAdapter(regionesAdapter);

        spRegiones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView seleccion = (TextView) view;
                if(!seleccion.getText().toString().equals(SpinnerUtilidad.SPINNER_ENTRADA)){
                    spComandos.setEnabled(true);
                    Region region = Region.find(Region.class,"regional=?",seleccion.getText().toString()).get(0);
                    configurarSpinnerDeComandos(region);
                }
                else{
                    spComandos.setAdapter(null);
                    spComandos.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void configurarSpinnerDeComandos(Region region) {
        List<ComandoDePolicia> comandoDePolicias = ComandoDePolicia.find(ComandoDePolicia.class,"region=?",region.getId().toString());
        ArrayList<String> comandos = SpinnerUtilidad.obtenerListaDeNombres(comandoDePolicias);
        ArrayAdapter comandosAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, comandoDePolicias);
        comandosAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spComandos.setAdapter(comandosAdapter);

        spComandos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView seleccion = (TextView) view;
                if(!seleccion.getText().toString().equals(SpinnerUtilidad.SPINNER_ENTRADA)){
                    comando = ComandoDePolicia.find(ComandoDePolicia.class,"nombre=?",seleccion.getText().toString()).get(0);
                    mapa.ajustarPosicionPorComando(comando);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void verEstadoDeCuadrante(){
        Intent estadoDeCuadrante = new Intent(this,MenuEstadoDelCuadranteActivity.class);
        estadoDeCuadrante.putExtra("cuadranteId",cuadrante.getId().toString());
        startActivity(estadoDeCuadrante);
        finish();
    }

}
