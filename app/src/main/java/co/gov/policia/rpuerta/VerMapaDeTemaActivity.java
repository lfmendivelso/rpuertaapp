package co.gov.policia.rpuerta;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.logica.MapaDeEstado;
import co.gov.policia.rpuerta.modelo.Ciudadano;
import co.gov.policia.rpuerta.modelo.OpcionDeRespuesta;
import co.gov.policia.rpuerta.modelo.Pregunta;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.modelo.Respuesta;
import co.gov.policia.rpuerta.modelo.Seccion;
import co.gov.policia.rpuerta.utilidad.SpinnerUtilidad;

public class VerMapaDeTemaActivity extends AppCompatActivity {

    public static final String TITULO_MIEDO = "Mapa de Zonas de Miedo";
    public static final String TITULO_VICTIMIZACION = "Mapa de Zonas de Victimizacion";
    public static final String TITULO_APOYO = "Mapa de Zonas de Apoyo Social";
    public static final String TITULO_EVALUACION = "Mapa de Zonas de Evualizacion de Servicio";

    public static final Integer SECCION_MIEDO=6;
    public static final Integer SECCION_VICTIMIZACION=5;
    public static final Integer SECCION_APOYO=3;
    public static final Integer SECCION_EVALUACION=1;


    private MapaDeEstado mapaDeEstado;
    private ReportePAP reporte;
    private TextView titulo;
    private Spinner spCategorias;
    private Integer tipoDeMapa;
    private List<Ciudadano> ciudadanos;
    private List<Pregunta> preguntas;
    private Integer seccionActual;
    private LinearLayout llyOpciones;
    private ScrollView scrollOpciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_mapa_de_tema);
        String cuadranteId = getIntent().getStringExtra("cuadranteId");
        String version = getIntent().getStringExtra("version");
        reporte = ReportePAP.find(ReportePAP.class, "cuadrante=? and version=?", cuadranteId, version).get(0);
        ciudadanos = Ciudadano.find(Ciudadano.class, "reporte=?", reporte.getId().toString());
        llyOpciones = (LinearLayout) findViewById(R.id.ly_vermapa_opciones);
        scrollOpciones = (ScrollView) findViewById(R.id.scroll_vermapa_opciones);

        titulo = (TextView) findViewById(R.id.lb_vermapa_titulo);
        spCategorias = (Spinner) findViewById(R.id.sp_vermapa_categoria);

        tipoDeMapa = Integer.parseInt(getIntent().getStringExtra("tipo"));
        mapaDeEstado = new MapaDeEstado((MapView) findViewById(R.id.mp_vermapa_mapa),reporte);
        ajustarInformacion(tipoDeMapa);
        ajustarEventosDeSpinner();
    }

    private void ajustarInformacion(Integer tipo) {
        spCategorias.setAdapter(null);
        if(tipo == MenuEstadoDelCuadranteActivity.MIEDO){
            titulo.setText(TITULO_MIEDO);
            seccionActual = SECCION_MIEDO;
            ajustarDatosDeSpinnerDeCategorias(true);
        }
        else if(tipo==MenuEstadoDelCuadranteActivity.VICTIMIZACION){
            titulo.setText(TITULO_VICTIMIZACION);
            seccionActual = SECCION_VICTIMIZACION;
            ajustarDatosDeSpinnerDeCategorias(false);
        }
        else if(tipo==MenuEstadoDelCuadranteActivity.APOYO){
            titulo.setText(TITULO_APOYO);
            seccionActual = SECCION_APOYO;
            ajustarDatosDeSpinnerDeCategorias(false);
        }
        else if(tipo==MenuEstadoDelCuadranteActivity.EVALUACION){
            titulo.setText(TITULO_EVALUACION);
            seccionActual = SECCION_EVALUACION;
            ajustarDatosDeSpinnerDeCategorias(false);

        }
        else {
            titulo.setText("Mapa de Estado");
        }
    }

    public void ajustarEventosDeSpinner(){
        spCategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView categoria = (TextView) view;
                Pregunta pregunta = obtenerPregunta(categoria.getText().toString());
                List<Respuesta> respuestas = buscarRepuestas(pregunta);
                mapaDeEstado.procesarRespuestas(respuestas);
                generarOpciones(pregunta);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void generarOpciones(Pregunta pregunta){
        llyOpciones.removeAllViews();
        for(OpcionDeRespuesta opcionDeRespuesta : pregunta.getTipoDePregunta().getOpciones()){
            TextView dato = new TextView(this);
            dato.setText(opcionDeRespuesta.getOpcion());
            dato.setBackgroundColor(Color.parseColor(opcionDeRespuesta.getColor()));
            llyOpciones.addView(dato);
        }
        scrollOpciones.invalidate();
        llyOpciones.invalidate();
    }

    public void verMapaCuadranteRegresar(View view){
        finish();
    }

    public List<Respuesta> buscarRepuestas(Pregunta pregunta){
        ArrayList<Respuesta> respuestas = new ArrayList<>();
        for(Ciudadano ciudadano : ciudadanos){
            List<Respuesta> consulta = Respuesta.find(Respuesta.class, "ciudadano=? and pregunta=?", ciudadano.getId().toString(), pregunta.getId().toString());
            if (!consulta.isEmpty()) {
                respuestas.add(consulta.get(0));
            }
        }
        return respuestas;
    }

    private void ajustarDatosDeSpinnerDeCategorias(Boolean miedo){
        Seccion seccion = Seccion.findById(Seccion.class,seccionActual);
        if(seccionActual.equals(SECCION_MIEDO)){
            seccion = Seccion.findById(Seccion.class,seccionActual-1);
        }
        preguntas = seccion.getPreguntas();
        filtrarPreguntas(miedo);
        ArrayList<String> listaDeDatos = SpinnerUtilidad.obtenerListaDePreguntas(preguntas);
        ArrayAdapter datos = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listaDeDatos);
        datos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategorias.setAdapter(datos);
    }

    private Pregunta obtenerPregunta(String texto){
        Pregunta pregunta = null;
        Boolean encontrado=false;
        for(int i=0;i<preguntas.size() && encontrado==false;i++){
            if(preguntas.get(i).getAyuda().equals(texto)){
                pregunta = preguntas.get(i);
                encontrado=true;
            }
        }
        return pregunta;
    }

    private void filtrarPreguntas(Boolean miedo){
        ArrayList<Pregunta> preguntasTemp = new ArrayList<>();
        if(miedo==false) {
            for (int i = 0; i < preguntas.size(); i++) {
                if (!preguntas.get(i).getAyuda().equals("Miedo") && miedo == false) {
                    preguntasTemp.add(preguntas.get(i));
                }
            }
        }
        else{
            Boolean encontrado = false;
            for(int i=0;i<preguntas.size() && encontrado==false;i++){
                if(preguntas.get(i).getAyuda().equals("Miedo")){
                    preguntasTemp.add(preguntas.get(i));
                    encontrado=true;
                }
            }
        }
        preguntas = preguntasTemp;
    }


}
