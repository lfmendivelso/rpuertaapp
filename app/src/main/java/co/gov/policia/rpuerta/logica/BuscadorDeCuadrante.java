package co.gov.policia.rpuerta.logica;


import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import co.gov.policia.rpuerta.logica.interfaces.Mapa;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class BuscadorDeCuadrante extends AsyncTask< String , Context, Void > {

    private Context context;
    private Cuadrante cuadrante;
    private Double latitud;
    private Double longitud;
    private TextView mensaje;
    private ComandoDePolicia comando;
    private Mapa mapa;

    public BuscadorDeCuadrante(Context context, Cuadrante cuadrante, TextView mensaje, Double latitud, Double longitud, Mapa mapa) {
        this.context = context;
        this.mensaje=mensaje;
        this.latitud=latitud;
        this.longitud=longitud;
        this.comando=null;
        this.cuadrante=cuadrante;
        this.mapa = mapa;
    }

    public BuscadorDeCuadrante(Context context,Cuadrante cuadrante,TextView mensaje,Double latitud,Double longitud,Mapa mapa,ComandoDePolicia comando) {
        this.context = context;
        this.mensaje=mensaje;
        this.latitud=latitud;
        this.longitud=longitud;
        this.comando=comando;
        this.cuadrante=cuadrante;
        this.mapa = mapa;
    }

    @Override
    protected void onPreExecute() {
        mensaje.setText("Buscando Cuadrante");
    }

    @Override
    protected Void doInBackground(String... params) {
        if(comando!=null){
            cuadrante = new BusquedaDeCuadrante(latitud,longitud,comando).buscar();
        }
        else{
            cuadrante = new BusquedaDeCuadrante(latitud,longitud).buscar();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if(cuadrante!=null){
            if(comando!=null){
                mensaje.setText(cuadrante.getCuadrante());
            }
            else{
                mensaje.setText("Cuadrante: "+cuadrante.getCuadrante());
            }
            mapa.agregarZonaDelCuadrante(cuadrante);
        }
        else{
            mensaje.setText("Cuadrante No Encontrado.");
        }
    }

    public Cuadrante getCuadrante() {
        return cuadrante;
    }
}