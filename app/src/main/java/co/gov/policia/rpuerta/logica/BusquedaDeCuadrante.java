package co.gov.policia.rpuerta.logica;

import java.util.List;

import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.utilidad.Ecuaciones;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class BusquedaDeCuadrante {

    private Double latitud;
    private Double longitud;
    private ComandoDePolicia comandoDePolicia;

    public BusquedaDeCuadrante(Double latitud, Double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.comandoDePolicia = null;
    }

    public BusquedaDeCuadrante(Double latitud, Double longitud, ComandoDePolicia comandoDePolicia) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.comandoDePolicia = comandoDePolicia;
    }

    public Cuadrante buscar(){
        Cuadrante buscado = null;
        Boolean encontrado = false;
        Double distancia = 0.000000000000000;
        List<Cuadrante> cuadrantes = obtenerCuadrante();
        for(int i=0;i<cuadrantes.size() && encontrado==false;i++){
            Cuadrante cuadrante = cuadrantes.get(i);
            if(Ecuaciones.coordenadaDentroDelCuadrante(cuadrante,latitud,longitud)){
                if(buscado==null){
                    buscado=cuadrante;
                    distancia = Ecuaciones.distanciaEntreCoordenadas(cuadrante,latitud,longitud);
                }
                else{
                    Double distanciaDeCentro = Ecuaciones.distanciaEntreCoordenadas(cuadrante,latitud,longitud);
                    if(distanciaDeCentro<distancia){
                        buscado=cuadrante;
                        distancia = distanciaDeCentro;
                    }
                }
            }
        }
        return buscado;
    }



    private List<Cuadrante> obtenerCuadrante(){
        if(comandoDePolicia != null){
            return Cuadrante.find(Cuadrante.class,"comando=?",comandoDePolicia.getId().toString());
        }
        return Cuadrante.listAll(Cuadrante.class);
    }

}
