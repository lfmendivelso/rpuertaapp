package co.gov.policia.rpuerta.logica;

import android.graphics.Color;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polygon;

import java.util.List;

import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.modelo.OpcionDeRespuesta;
import co.gov.policia.rpuerta.modelo.Pregunta;
import co.gov.policia.rpuerta.modelo.ReportePAP;
import co.gov.policia.rpuerta.modelo.Respuesta;
import co.gov.policia.rpuerta.modelo.TipoDePregunta;
import co.gov.policia.rpuerta.utilidad.PaletaDeColores;

/**
 * Created by lfmendivelso on 27/12/16.
 */

public class MapaDeEstado {

    private MapView mapa;
    private IMapController controlador;
    private GeoPoint centroDelCuadrante;
    private Cuadrante cuadrante;

    public MapaDeEstado(MapView mapa, ReportePAP reporte) {
        this.mapa = mapa;
        this.cuadrante = reporte.getCuadrante();
        configurarMapa();
    }

    private void configurarMapa() {
        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.getTileProvider().clearTileCache();
        controlador = mapa.getController();
        controlador.setZoom(16);
        mapa.setBuiltInZoomControls(true);
        mapa.setMultiTouchControls(true);
        centroDelCuadrante = new GeoPoint(cuadrante.getLatitud(), cuadrante.getLongitud());
        controlador.setCenter(centroDelCuadrante);
    }

    public void procesarRespuestas(List<Respuesta> respuestas) {
        mapa.getOverlays().clear();
        mapa.invalidate();
        if(!respuestas.isEmpty()) {
            Pregunta pregunta = Pregunta.findById(Pregunta.class,respuestas.get(0).getId());
            TipoDePregunta tipoDePregunta = pregunta.getTipoDePregunta();
            List<OpcionDeRespuesta> opciones = tipoDePregunta.getOpciones();
            for (Respuesta respuesta : respuestas) {

                GeoPoint posicion = new GeoPoint(respuesta.getLatitud(), respuesta.getLongitud());
                Polygon zona = new Polygon();
                zona.setPoints(Polygon.pointsAsCircle(posicion, 25));
                if(pregunta.getId()==19){
                    zona.setFillColor(PaletaDeColores.ROJO_TRANSPARENCIA);
                    zona.setStrokeColor(PaletaDeColores.ROJO_TRANSPARENCIA);
                    zona.setTitle("Zona de Peligro");
                }
                else{
                    zona.setFillColor(colorPorOpcionDeRespuesta(respuesta.getRespuestaPorOpcion()));
                    zona.setStrokeColor(colorPorOpcionDeRespuesta(respuesta.getRespuestaPorOpcion()));
                    zona.setTitle(respuesta.getRespuestaPorOpcion().getOpcion());
                }

                zona.setStrokeWidth(2);

                mapa.getOverlays().add(zona);
            }
        }
    }

    private Integer colorPorOpcionDeRespuesta(OpcionDeRespuesta opcionDeRespuesta){
        return Color.parseColor(opcionDeRespuesta.getColor());
    }
}
