package co.gov.policia.rpuerta.logica;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.Polygon;
import java.util.ArrayList;
import co.gov.policia.rpuerta.logica.interfaces.Mapa;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.utilidad.PaletaDeColores;
import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class MapaGPS implements MapEventsReceiver,LocationListener,Mapa {

    // Constantes
    public static final long REFRESH_TIME = 500;
    public static final float REFRESH_DISTANCE = 2;

    //Mapa
    private Context contexto;
    private MapView mapa;
    private IMapController controlador;
    private Double latitud;
    private Double longitud;
    private GeoPoint localizacionActual;
    private ItemizedIconOverlay<OverlayItem> marcador;
    private MapEventsOverlay eventosDelMapa;
    private LocationManager localizador;


    public MapaGPS(Context contexto, MapView mapa) {
        this.contexto = contexto;
        this.mapa = mapa;
        this.latitud=0.000000000000000;
        this.longitud=0.000000000000000;
        ajustarLocalizador();
        configurarMapa();
    }

    private void configurarMapa() {
        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.getTileProvider().clearTileCache();
        controlador = mapa.getController();
        controlador.setZoom(16);
        mapa.setBuiltInZoomControls(true);
        mapa.setMultiTouchControls(true);
        eventosDelMapa = new MapEventsOverlay(contexto, this);
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    public void ajustarLocalizador() {
        localizador = (LocationManager) contexto.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        localizador.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, REFRESH_TIME, REFRESH_DISTANCE, this);
    }

    public void centrarMapa() {
        localizacionActual = new GeoPoint(latitud, longitud);
        controlador.setCenter(localizacionActual);
    }

    public void cambiarMarcadorALocalizacionActual() {
        ArrayList<OverlayItem> itemsDelMarcador = new ArrayList<>();
        OverlayItem currentLocationMarker = new OverlayItem("Localizacion actual", "", localizacionActual);
        itemsDelMarcador.add(currentLocationMarker);
        marcador = new ItemizedIconOverlay<>(contexto, itemsDelMarcador, null);
        mapa.getOverlays().clear();
        mapa.getOverlays().add(0, eventosDelMapa);
        mapa.getOverlays().add(marcador);
    }

    @Override
    public void ajustarPosicionPorComando(ComandoDePolicia comando) {
        latitud = comando.getLatitud();
        longitud = comando.getLongitud();
        controlador.setZoom(comando.getZoom());
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    @Override
    public void agregarZonaDelCuadrante(Cuadrante cuadrante) {
        latitud = cuadrante.getLatitud();
        longitud = cuadrante.getLongitud();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        GeoPoint ubicacion = new GeoPoint(cuadrante.getLatitud(),cuadrante.getLongitud());

        Polygon zona = new Polygon(contexto);
        Double radio = Math.sqrt(cuadrante.calcularRadio2())*7500;
        zona.setPoints(Polygon.pointsAsCircle(ubicacion, radio));
        zona.setFillColor(PaletaDeColores.ROJO_TRANSPARENCIA);
        zona.setStrokeColor(PaletaDeColores.ROJO_TRANSPARENCIA);
        zona.setStrokeWidth(2);

        mapa.getOverlays().add(zona);
        mapa.invalidate();
    }

    public Double getLatitud() {
        return latitud;
    }

    public Double getLongitud() {
        return longitud;
    }


    @Override
    public boolean singleTapConfirmedHelper(GeoPoint geoPoint) {
        latitud = geoPoint.getLatitude();
        longitud = geoPoint.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint geoPoint) {
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        localizador = (LocationManager) contexto.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        localizador.requestLocationUpdates(LocationManager.GPS_PROVIDER, REFRESH_TIME, REFRESH_DISTANCE, this);

    }


}
