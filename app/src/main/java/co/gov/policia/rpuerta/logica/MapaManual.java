package co.gov.policia.rpuerta.logica;

import android.content.Context;
import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polygon;
import co.gov.policia.rpuerta.logica.interfaces.Mapa;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;
import co.gov.policia.rpuerta.utilidad.PaletaDeColores;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class MapaManual implements MapEventsReceiver,Mapa {

    //Mapa
    private Context contexto;
    private MapView mapa;
    private IMapController controlador;
    private Double latitud;
    private Double longitud;
    private GeoPoint localizacionActual;
    private Marker marcador;
    private MapEventsOverlay eventosDelMapa;


    public MapaManual(Context contexto, MapView mapa, Double latitud, Double longitud) {
        this.contexto = contexto;
        this.mapa = mapa;
        this.latitud = latitud;
        this.longitud = longitud;
        configurarMapa();
    }

    public void centrarMapa() {
        localizacionActual = new GeoPoint(latitud, longitud);
        controlador.setCenter(localizacionActual);
    }

    public void cambiarMarcadorALocalizacionActual() {
        mapa.getOverlays().clear();
        mapa.getOverlays().add(0, eventosDelMapa);
        marcador = new Marker(mapa);
        marcador.setPosition(localizacionActual);
        mapa.getOverlays().add(marcador);
        mapa.invalidate();
    }

    private void configurarMapa() {
        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.getTileProvider().clearTileCache();
        controlador = mapa.getController();
        controlador.setZoom(8);
        mapa.setBuiltInZoomControls(true);
        mapa.setMultiTouchControls(true);
        eventosDelMapa = new MapEventsOverlay(contexto, this);
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    public void ajustarPosicionPorComando(ComandoDePolicia comando){
        latitud = comando.getLatitud();
        longitud = comando.getLongitud();
        controlador.setZoom(comando.getZoom());
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }

    public void agregarZonaDelCuadrante(Cuadrante cuadrante){
        latitud = cuadrante.getLatitud();
        longitud = cuadrante.getLongitud();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        GeoPoint ubicacion = new GeoPoint(cuadrante.getLatitud(),cuadrante.getLongitud());

        Polygon zona = new Polygon(contexto);
        Double radio = Math.sqrt(cuadrante.calcularRadio2())*7500;
        zona.setPoints(Polygon.pointsAsCircle(ubicacion, radio));
        zona.setFillColor(PaletaDeColores.ROJO_TRANSPARENCIA);
        zona.setStrokeColor(PaletaDeColores.ROJO_TRANSPARENCIA);
        zona.setStrokeWidth(2);

        mapa.getOverlays().add(zona);
        mapa.invalidate();
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint geoPoint) {
        latitud = geoPoint.getLatitude();
        longitud = geoPoint.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint geoPoint) {
        return false;
    }

    public Double getLatitud() {
        return latitud;
    }

    public Double getLongitud() {
        return longitud;
    }
}
