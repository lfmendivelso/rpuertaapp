package co.gov.policia.rpuerta.logica;

import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.view.ViewGroup;
import android.widget.EditText;
import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import java.util.ArrayList;

/**
 * Created by lfmendivelso on 23/12/16.
 */

public class RespuestaTipoMapa implements MapEventsReceiver {

    //Mapa
    private Context contexto;
    private MapView mapa;
    private IMapController controlador;
    private Double latitud;
    private Double longitud;
    private GeoPoint localizacionActual;
    private MapEventsOverlay eventosDelMapa;
    private EditText referencia;

    public RespuestaTipoMapa(Context contexto,Double latitud,Double longitud){
        this.contexto = contexto;
        this.mapa = new MapView(this.contexto);
        this.referencia = new EditText(this.contexto);
        this.referencia.setHint("De una referencia del lugar");
        this.referencia.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        this.referencia.setHintTextColor(Color.GRAY);
        this.latitud =latitud;
        this.longitud = longitud;
        configurarMapa();
    }

    public void centrarMapa() {
        localizacionActual = new GeoPoint(latitud, longitud);
        controlador.setCenter(localizacionActual);
    }

    public void cambiarMarcadorALocalizacionActual() {
        ArrayList<OverlayItem> itemsDelMarcador = new ArrayList<>();
        OverlayItem currentLocationMarker = new OverlayItem("Localizacion actual","",localizacionActual);
        itemsDelMarcador.add(currentLocationMarker);
        ItemizedIconOverlay<OverlayItem> marcador = new ItemizedIconOverlay<>(contexto, itemsDelMarcador, null);
        mapa.getOverlays().clear();
        mapa.getOverlays().add(0,eventosDelMapa);
        mapa.getOverlays().add(marcador);
    }

    private void configurarMapa(){
        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.getTileProvider().clearTileCache();
        mapa.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 420));
        controlador = mapa.getController();
        controlador.setZoom(16);
        mapa.setBuiltInZoomControls(true);
        mapa.setMultiTouchControls(true);
        eventosDelMapa = new MapEventsOverlay(contexto,this);
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
    }


    @Override
    public boolean singleTapConfirmedHelper(GeoPoint geoPoint) {
        latitud = geoPoint.getLatitude();
        longitud = geoPoint.getLongitude();
        centrarMapa();
        cambiarMarcadorALocalizacionActual();
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint geoPoint) {
        return false;
    }

    public MapView getMapa() {
        return mapa;
    }

    public void setMapa(MapView mapa) {
        this.mapa = mapa;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public EditText getReferencia() {
        return referencia;
    }

    public void setReferencia(EditText referencia) {
        this.referencia = referencia;
    }
}
