package co.gov.policia.rpuerta.logica.bd;

import android.content.Context;

import co.gov.policia.rpuerta.logica.bd.data.CuadranteData;
import co.gov.policia.rpuerta.logica.bd.data.RegionData;
import co.gov.policia.rpuerta.logica.bd.data.RelacionConVecinosData;
import co.gov.policia.rpuerta.logica.bd.data.SeccionData;
import co.gov.policia.rpuerta.logica.bd.data.TiempoDeResidenciaData;
import co.gov.policia.rpuerta.logica.bd.data.TipoDeDocumentoData;
import co.gov.policia.rpuerta.logica.bd.data.TipoDePreguntasData;
import co.gov.policia.rpuerta.logica.bd.data.TipoDeViviendaData;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class ConfiguracionBD {

    private Context context;

    public ConfiguracionBD(Context context){
        this.context = context;
    }

    public void procesarData(){
        new TiempoDeResidenciaData(context).procesar();
        new TipoDeViviendaData(context).procesar();
        new TipoDeDocumentoData(context).procesar();
        new RelacionConVecinosData(context).procesar();
        new TipoDePreguntasData(context).procesar();
        new SeccionData(context).procesar();
        new RegionData(context).procesar();
        new CuadranteData(context).procesar();
    }
}
