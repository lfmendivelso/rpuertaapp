package co.gov.policia.rpuerta.logica.bd;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class Configurador extends AsyncTask< String , Context, Void > {

    private ProgressDialog ventanaEmergente;
    private Context context;

    public Configurador(Context context) {
        this.context = context;
        ventanaEmergente = new ProgressDialog(context);
        ventanaEmergente.setCancelable(false);
        ventanaEmergente.setMessage("Cargando Base de Datos...");
        ventanaEmergente.setTitle("Por favor Espere");
        ventanaEmergente.setIndeterminate(true);
    }

    @Override
    protected void onPreExecute() {
        ventanaEmergente.show();
    }

    @Override
    protected Void doInBackground(String... params) {
        // Do Your WORK here
        ConfiguracionBD configuracionBD = new ConfiguracionBD(context);
        configuracionBD.procesarData();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (ventanaEmergente != null && ventanaEmergente.isShowing()) {
            ventanaEmergente.dismiss();
        }
    }
}