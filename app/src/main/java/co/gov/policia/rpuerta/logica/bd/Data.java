package co.gov.policia.rpuerta.logica.bd;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public abstract class Data {

    private Context context;
    protected Gson gson;

    public Data(){
        this.gson = new Gson();
    }

    public String convertirJSONaString(String jsonPath){
        try{
            InputStream unidadesJsonData = context.getAssets().open(jsonPath);
            StringWriter writer = new StringWriter();
            IOUtils.copy(unidadesJsonData, writer, "UTF8");
            return writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Error";
        }
    }

    public JsonObject convertirStringJsonAJsonObject(String jsonString){
        return new JsonParser().parse(jsonString).getAsJsonObject();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public abstract void cargar();

    public abstract Boolean verificar();

    public void  procesar(){
        if(verificar()){
            System.out.println("Iniciar Carga");
            cargar();
        }
    }
}
