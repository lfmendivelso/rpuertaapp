package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class CuadranteData extends Data {

    public static final String JSON = "Cuadrantes.json";
    public static final String ARREGLO = "cuadrantes";

    public CuadranteData(Context context){
        setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    Cuadrante data = gson.fromJson(record,Cuadrante.class);
                    String comando = data.getCuadrante().substring(0,5);
                    List<ComandoDePolicia> comandoDePolicia = ComandoDePolicia.find(ComandoDePolicia.class,"unidad=?",comando);
                    if(comandoDePolicia.size()>0){
                        data.setComando(comandoDePolicia.get(0));
                    }
                    data.save();
                    System.out.println(data.toString());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(Cuadrante.count(Cuadrante.class)==0){
            return true;
        }
        return false;
    }
}
