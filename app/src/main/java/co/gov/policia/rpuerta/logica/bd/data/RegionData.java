package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Region;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class RegionData extends Data {

    public static final String JSON = "RegionalesDePolicia.json";
    public static final String ARREGLO = "regionales";

    public RegionData(Context context){
        setContext(context);
    }


    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    Region data = gson.fromJson(record,Region.class);
                    System.out.println(data.toString());
                    data.save();

                    List<ComandoDePolicia> comandos = new ArrayList<>();
                    JsonArray comandosData = record.get("comandos").getAsJsonArray();
                    for(int j = 0;j<comandosData.size();j++){
                        JsonObject preguntaData = comandosData.get(j).getAsJsonObject();
                        ComandoDePolicia comando = gson.fromJson(preguntaData,ComandoDePolicia.class);
                        comando.setRegion(data);
                        comando.save();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(Region.count(Region.class)==0){
            return true;
        }
        return false;
    }
}
