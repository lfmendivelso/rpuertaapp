package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.RelacionConVecinos;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class RelacionConVecinosData extends Data {

    public static final String JSON = "RelacionConVecinos.json";
    public static final String ARREGLO = "relacionesConVecinos";

    public RelacionConVecinosData(Context context){
        setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    RelacionConVecinos data = gson.fromJson(record,RelacionConVecinos.class);
                    data.save();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(RelacionConVecinos.count(RelacionConVecinos.class)==0){
            return true;
        }
        return false;
    }
}
