package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.Pregunta;
import co.gov.policia.rpuerta.modelo.Seccion;
import co.gov.policia.rpuerta.modelo.TipoDeDocumento;
import co.gov.policia.rpuerta.modelo.TipoDePregunta;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class SeccionData extends Data {

    public static final String JSON = "Secciones.json";
    public static final String ARREGLO = "secciones";

    public SeccionData(Context context){
        setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    Seccion data = gson.fromJson(record,Seccion.class);
                    System.out.println(data.toString());
                    data.save();

                    JsonArray preguntasData = record.get("preguntas").getAsJsonArray();
                    for(int j = 0;j<preguntasData.size();j++){
                        JsonObject preguntaData = preguntasData.get(j).getAsJsonObject();
                        Pregunta pregunta = gson.fromJson(preguntaData,Pregunta.class);
                        Long tipoId = preguntaData.get("tipo").getAsLong();
                        TipoDePregunta tipo = TipoDePregunta.findById(TipoDePregunta.class,tipoId);
                        pregunta.setTipoDePregunta(tipo);
                        pregunta.setSeccion(data);
                        pregunta.save();
                    }
                }
            }
            List<Seccion> seccions = Seccion.listAll(Seccion.class);
            for(Seccion s : seccions){
                System.out.println(s.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(Seccion.count(Seccion.class)==0){
            return true;
        }
        return false;
    }
}
