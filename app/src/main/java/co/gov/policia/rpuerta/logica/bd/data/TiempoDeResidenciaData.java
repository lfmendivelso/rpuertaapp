package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.TiempoDeResidencia;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TiempoDeResidenciaData extends Data {

    public static final String JSON = "TiempoDeResidencia.json";
    public static final String ARREGLO = "tiemposDeResidencia";

    public TiempoDeResidenciaData(Context context){
        this.setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    TiempoDeResidencia data = gson.fromJson(record,TiempoDeResidencia.class);
                    data.save();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        System.out.println("Inicia Verificación: "+getClass().getName());
        if(TiempoDeResidencia.count(TiempoDeResidencia.class)==0){
            System.out.println("Carga: "+getClass().getName());
            return true;
        }
        return false;
    }
}
