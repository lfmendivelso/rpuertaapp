package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.TipoDeDocumento;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TipoDeDocumentoData extends Data {

    public static final String JSON = "TipoDeDocumento.json";
    public static final String ARREGLO = "tiposDeDocumento";

    public TipoDeDocumentoData(Context context){
        setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    TipoDeDocumento data = gson.fromJson(record,TipoDeDocumento.class);
                    data.save();
                    System.out.println(data.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(TipoDeDocumento.count(TipoDeDocumento.class)==0){
            return true;
        }
        return false;
    }
}
