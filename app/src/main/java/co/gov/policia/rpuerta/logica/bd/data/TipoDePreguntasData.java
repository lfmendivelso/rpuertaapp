package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.OpcionDeRespuesta;
import co.gov.policia.rpuerta.modelo.TipoDePregunta;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TipoDePreguntasData extends Data {

    public static final String JSON = "TipoDePreguntas.json";
    public static final String ARREGLO = "tiposDePregunta";

    public TipoDePreguntasData(Context context){
        setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();

                    TipoDePregunta data = gson.fromJson(record,TipoDePregunta.class);
                    data.save();
                    if(record.get("opciones")!= null){
                        JsonArray opciones = record.get("opciones").getAsJsonArray();
                        for(int j = 0;j<opciones.size();j++){
                            JsonObject opcionData = opciones.get(j).getAsJsonObject();
                            OpcionDeRespuesta opcion = gson.fromJson(opcionData,OpcionDeRespuesta.class);
                            opcion.setTipo(data);
                            opcion.save();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar() {
        if(TipoDePregunta.count(TipoDePregunta.class)==0){
            return true;
        }
        return false;

    }
}
