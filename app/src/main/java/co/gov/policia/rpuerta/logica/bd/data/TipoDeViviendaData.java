package co.gov.policia.rpuerta.logica.bd.data;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import co.gov.policia.rpuerta.logica.bd.Data;
import co.gov.policia.rpuerta.modelo.TipoDeVivienda;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TipoDeViviendaData extends Data {

    public static final String JSON = "TipoDeVivienda.json";
    public static final String ARREGLO = "tiposDeVivienda";

    public TipoDeViviendaData(Context context){
        this.setContext(context);
    }

    @Override
    public void cargar() {
        try {
            String jsonAsString = convertirJSONaString(JSON);
            JsonObject jsonObject = convertirStringJsonAJsonObject(jsonAsString);
            if(jsonObject!=null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray(ARREGLO);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject record = jsonArray.get(i).getAsJsonObject();
                    TipoDeVivienda data = gson.fromJson(record,TipoDeVivienda.class);
                    data.save();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean verificar()
    {
        if(TipoDeVivienda.count(TipoDeVivienda.class)==0){
            return true;
        }
        return false;
    }
}
