package co.gov.policia.rpuerta.logica.interfaces;

import co.gov.policia.rpuerta.modelo.ComandoDePolicia;
import co.gov.policia.rpuerta.modelo.Cuadrante;

/**
 * Created by lfmendivelso on 26/12/16.
 */

public interface Mapa {

    public void cambiarMarcadorALocalizacionActual();
    public void ajustarPosicionPorComando(ComandoDePolicia comando);
    public void agregarZonaDelCuadrante(Cuadrante cuadrante);

}
