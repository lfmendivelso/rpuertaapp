package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class Ciudadano extends SugarRecord {

    private ReportePAP reporte;
    private String documento;
    private TipoDeDocumento tipoDeDocumento;
    private String correo;
    private String direccion;
    private Double longitud;
    private Double latitud;
    private Boolean propietario;
    private Integer numeroResidentes;
    private TipoDeVivienda tipoDeVivienda;
    private TiempoDeResidencia tiempoDeResidencia;
    private RelacionConVecinos relacionConVecinos;

    public Ciudadano() {
        this.reporte = null;
        this.documento = "No Data";
        this.tipoDeDocumento = null;
        this.correo = "No Data";
        this.direccion = "No Data";
        this.longitud = 0.00000000000000000;
        this.latitud = 0.00000000000000000;
        this.propietario = false;
        this.numeroResidentes = 0;
        this.tipoDeVivienda = null;
        this.tiempoDeResidencia = null;
        this.relacionConVecinos = null;
    }

    public ReportePAP getReporte() {
        return reporte;
    }

    public void setReporte(ReportePAP reporte) {
        this.reporte = reporte;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public TipoDeDocumento getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Boolean getPropietario() {
        return propietario;
    }

    public void setPropietario(Boolean propietario) {
        this.propietario = propietario;
    }

    public Integer getNumeroResidentes() {
        return numeroResidentes;
    }

    public void setNumeroResidentes(Integer numeroResidentes) {
        this.numeroResidentes = numeroResidentes;
    }

    public TipoDeVivienda getTipoDeVivienda() {
        return tipoDeVivienda;
    }

    public void setTipoDeVivienda(TipoDeVivienda tipoDeVivienda) {
        this.tipoDeVivienda = tipoDeVivienda;
    }

    public TiempoDeResidencia getTiempoDeResidencia() {
        return tiempoDeResidencia;
    }

    public void setTiempoDeResidencia(TiempoDeResidencia tiempoDeResidencia) {
        this.tiempoDeResidencia = tiempoDeResidencia;
    }

    public RelacionConVecinos getRelacionConVecinos() {
        return relacionConVecinos;
    }

    public void setRelacionConVecinos(RelacionConVecinos relacionConVecinos) {
        this.relacionConVecinos = relacionConVecinos;
    }

    @Override
    public String toString() {
        return "Ciudadano{" +
                "id=" + getId() +
                ", reporte=" + reporte +
                ", documento='" + documento + '\'' +
                ", tipoDeDocumento=" + tipoDeDocumento +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                ", propietario=" + propietario +
                ", numeroResidentes=" + numeroResidentes +
                ", tipoDeVivienda=" + tipoDeVivienda +
                ", tiempoDeResidencia=" + tiempoDeResidencia +
                ", relacionConVecinos=" + relacionConVecinos +
                '}';
    }
}
