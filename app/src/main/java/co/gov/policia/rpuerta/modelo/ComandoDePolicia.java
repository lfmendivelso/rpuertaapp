package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lfmendivelso on 25/12/16.
 */
public class ComandoDePolicia extends SugarRecord{

    private String unidad;
    private String nombre;
    private Double latitud;
    private Double longitud;
    private Integer zoom;
    private Region region;
    private List<Cuadrante> cuadrantes;

    public ComandoDePolicia() {
        this.unidad = "NODATA";
        this.nombre = "No Asignado";
        this.latitud = 0.000000000000000000;
        this.longitud = 0.000000000000000000;
        this.zoom=7;
        this.cuadrantes = new ArrayList<>();
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Integer getZoom() {
        return zoom;
    }

    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    public List<Cuadrante> getCuadrantes() {
        return cuadrantes;
    }

    public void setCuadrantes(List<Cuadrante> cuadrantes) {
        this.cuadrantes = cuadrantes;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
