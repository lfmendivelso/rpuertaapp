package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 25/12/16.
 */
public class Cuadrante extends SugarRecord{

    private String cuadrante;
    private Double latitud;
    private Double longitud;
    private Double area;
    private ComandoDePolicia comando;

    public Cuadrante() {
        this.cuadrante = "NOASIGANDO";
        this.latitud = 0.000000000000000000;
        this.longitud = 0.000000000000000000;
        this.area = 0.000000000000000000;
        this.comando = null;
    }

    public String getCuadrante() {
        return cuadrante;
    }

    public void setCuadrante(String cuadrante) {
        this.cuadrante = cuadrante;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public ComandoDePolicia getComando() {
        return comando;
    }

    public void setComando(ComandoDePolicia comando) {
        this.comando = comando;
    }

    public Double calcularRadio2() {
        return Math.sqrt(area/Math.PI);
    }

    @Override
    public String toString() {
        return cuadrante;
    }
}
