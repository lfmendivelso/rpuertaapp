package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class OpcionDeRespuesta extends SugarRecord {

    private String opcion;
    private TipoDePregunta tipo;
    private String color;

    public OpcionDeRespuesta() {
        this.opcion = "No Data";
        this.tipo = null;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public TipoDePregunta getTipo() {
        return tipo;
    }

    public void setTipo(TipoDePregunta tipo) {
        this.tipo = tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return opcion;
    }
}
