package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class Pregunta extends SugarRecord {

    private String pregunta;
    private String ayuda;
    private String descripcion;
    private Boolean requiereMapa;
    private Boolean esObligatorio;
    private Seccion seccion;
    private TipoDePregunta tipoDePregunta;

    public Pregunta(){
        this.pregunta = "No Data";
        this.ayuda = "No Data";
        this.descripcion = "No Data";
        this.requiereMapa = false;
        this.esObligatorio = false;
        this.seccion = null;
        this.tipoDePregunta = null;
    }

    public Pregunta(Long id, String pregunta, String ayuda, String descripcion, Boolean requiereMapa, Boolean esObligatorio, Seccion seccion, TipoDePregunta tipoDePregunta) {
        this.setId(id);
        this.pregunta = pregunta;
        this.ayuda = ayuda;
        this.descripcion = descripcion;
        this.requiereMapa = requiereMapa;
        this.esObligatorio = esObligatorio;
        this.seccion = seccion;
        this.tipoDePregunta = tipoDePregunta;
    }

    @Override
    public String toString() {
        return "Pregunta{" +
                "id=" + getId() +
                ", pregunta='" + pregunta + '\'' +
                ", ayuda='" + ayuda + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", requiereMapa=" + requiereMapa +
                ", esObligatorio=" + esObligatorio +
                ", seccion=" + seccion +
                ", tipoDePregnta=" + tipoDePregunta +
                '}';
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getAyuda() {
        return ayuda;
    }

    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getRequiereMapa() {
        return requiereMapa;
    }

    public void setRequiereMapa(Boolean requiereMapa) {
        this.requiereMapa = requiereMapa;
    }

    public Boolean getEsObligatorio() {
        return esObligatorio;
    }

    public void setEsObligatorio(Boolean esObligatorio) {
        this.esObligatorio = esObligatorio;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public TipoDePregunta getTipoDePregunta() {

        return tipoDePregunta;
    }

    public void setTipoDePregunta(TipoDePregunta tipoDePregunta) {
        this.tipoDePregunta = tipoDePregunta;
    }
}
