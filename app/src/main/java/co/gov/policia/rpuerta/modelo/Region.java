package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lfmendivelso on 25/12/16.
 */

public class Region extends SugarRecord{

    private String regional;
    private List<ComandoDePolicia> comandos;

    public Region() {
        this.regional="NODATA";
        this.comandos = new ArrayList<>();
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public List<ComandoDePolicia> getComandos() {
        return comandos;
    }

    public void setComandos(List<ComandoDePolicia> comandos) {
        this.comandos = comandos;
    }

    @Override
    public String toString() {
        return regional;
    }
}
