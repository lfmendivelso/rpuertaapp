package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class RelacionConVecinos extends SugarRecord {

    private String relacion;

    public RelacionConVecinos(Long id, String relacion) {
        this.setId(id);
        this.relacion = relacion;
    }

    public RelacionConVecinos() {
        this.relacion = "No Data";
    }

    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    @Override
    public String toString() {
        return relacion;
    }
}
