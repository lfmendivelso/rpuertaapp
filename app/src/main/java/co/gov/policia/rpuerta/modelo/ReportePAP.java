package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class ReportePAP extends SugarRecord {

    private String version;
    private Cuadrante cuadrante;
    private List<Ciudadano> ciudadanos;

    public ReportePAP(Long id, String version, List<Ciudadano> ciudadanos) {
        this.setId(id);
        this.version = version;
        this.ciudadanos = ciudadanos;
    }

    public ReportePAP() {
        this.version = "No Data";
        this.ciudadanos = new ArrayList<>();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Ciudadano> getCiudadanos() {
        return ciudadanos;
    }

    public void setCiudadanos(List<Ciudadano> ciudadanos) {
        this.ciudadanos = ciudadanos;
    }

    public List<Ciudadano> obtenerTodosLosCiudadanos(){
        ciudadanos = Ciudadano.find(Ciudadano.class,"reporte_PAP=?",getId()+"");
        return ciudadanos;
    }

    public void agregarCiudadano(Ciudadano ciudadano){
        ciudadanos = Ciudadano.find(Ciudadano.class,"reporte=?",getId()+"");
        ciudadanos.add(ciudadano);
        this.update();
    }

    public Cuadrante getCuadrante() {
        return cuadrante;
    }

    public void setCuadrante(Cuadrante cuadrante) {
        this.cuadrante = cuadrante;
    }

    @Override
    public String toString() {
        return version;
    }
}
