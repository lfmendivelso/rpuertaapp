package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class Respuesta  extends SugarRecord {

    private Pregunta pregunta;
    private Ciudadano ciudadano;
    private OpcionDeRespuesta respuestaPorOpcion;
    private String respuestaAbierta;
    private Double longitud;
    private Double latitud;

    public Respuesta(Long id, Ciudadano ciudadano, OpcionDeRespuesta respuestaPorOpcion, String respuestaAbierta, Double longitud, Double latitud, Pregunta pregunta) {
        this.setId(id);
        this.ciudadano = ciudadano;
        this.respuestaPorOpcion = respuestaPorOpcion;
        this.respuestaAbierta = respuestaAbierta;
        this.longitud = longitud;
        this.latitud = latitud;
        this.pregunta = pregunta;
    }

    public Respuesta() {
        this.ciudadano = null;
        this.respuestaPorOpcion = null;
        this.respuestaAbierta = "No Data";
        this.longitud = 0.00000000000000000;
        this.latitud = 0.00000000000000000;
    }

    public Ciudadano getCiudadano() {
        return ciudadano;
    }

    public void setCiudadano(Ciudadano ciudadano) {
        this.ciudadano = ciudadano;
    }

    public OpcionDeRespuesta getRespuestaPorOpcion() {
        return respuestaPorOpcion;
    }

    public void setRespuestaPorOpcion(OpcionDeRespuesta respuestaPorOpcion) {
        this.respuestaPorOpcion = respuestaPorOpcion;
    }

    public String getRespuestaAbierta() {
        return respuestaAbierta;
    }

    public void setRespuestaAbierta(String respuestaAbierta) {
        this.respuestaAbierta = respuestaAbierta;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "id=" + getId() +
                ", ciudadano=" + ciudadano +
                ", respuestaPorOpcion=" + respuestaPorOpcion +
                ", respuestaAbierta='" + respuestaAbierta + '\'' +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                '}';
    }
}
