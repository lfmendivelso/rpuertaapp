package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class Seccion  extends SugarRecord {

    private String nombre;
    private String descripcion;
    private List<Pregunta> preguntas;


    public Seccion(Long id, String nombre, String descripcion, List<Pregunta> preguntas) {
        this.setId(id);
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.preguntas = preguntas;
    }

    public Seccion() {
        this.nombre = "No Data";
        this.descripcion = "No Data";
        this.preguntas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Pregunta> getPreguntas() {
        preguntas = Pregunta.find(Pregunta.class,"seccion=?",getId()+"");
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public void agregarPregunta(Pregunta pregunta){
        preguntas.add(pregunta);
    }

    @Override
    public String toString() {
        return "Seccion{" +
                "id=" + getId() +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
