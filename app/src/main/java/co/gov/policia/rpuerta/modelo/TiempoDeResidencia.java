package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TiempoDeResidencia  extends SugarRecord {

    private String tiempo;

    public TiempoDeResidencia(Long id, String tiempo) {
        this.setId(id);
        this.tiempo = tiempo;
    }

    public TiempoDeResidencia() {
        this.tiempo = "No Data";
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public String toString() {
        return tiempo;
    }
}
