package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TipoDeDocumento  extends SugarRecord {

    private String tipo;
    private String nombre;

    public TipoDeDocumento(Long id, String tipo, String nombre) {
        this.setId(id);
        this.tipo = tipo;
        this.nombre = nombre;
    }

    public TipoDeDocumento() {
        this.tipo = "No Data";
        this.nombre = "No Data";
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
