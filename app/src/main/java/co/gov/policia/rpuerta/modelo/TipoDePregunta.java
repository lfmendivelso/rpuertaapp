package co.gov.policia.rpuerta.modelo;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lfmendivelso on 10/12/16.
 */

public class TipoDePregunta  extends SugarRecord {

    private String tipo;
    private String descripcion;
    private List<OpcionDeRespuesta> opciones;

    public TipoDePregunta(Long id, String tipo, String descripcion, List<OpcionDeRespuesta> opciones) {
        this.setId(id);
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.opciones = opciones;
    }

    public TipoDePregunta() {
        this.tipo = "No Data";
        this.descripcion = "No Data";
        this.opciones = new ArrayList<>();
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<OpcionDeRespuesta> getOpciones() {
        opciones = OpcionDeRespuesta.find(OpcionDeRespuesta.class,"tipo=?",getId()+"");
        return opciones;
    }

    public void setOpciones(List<OpcionDeRespuesta> opciones) {
        opciones = OpcionDeRespuesta.find(OpcionDeRespuesta.class,"tipo=?",this.getId().toString());
        this.opciones = opciones;
    }

    public void agregarOpcion(OpcionDeRespuesta opcion){
        opciones.add(opcion);
    }

    @Override
    public String toString() {
        return tipo;
    }
}
