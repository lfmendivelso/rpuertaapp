package co.gov.policia.rpuerta.utilidad;

import co.gov.policia.rpuerta.modelo.Cuadrante;

/**
 * Created by lfmendivelso on 26/12/16.
 */

public class Ecuaciones {

    public static final double R = 6372.8;

    public static Double distanciaEntreCoordenadas(Cuadrante cuadrante, Double latitud, Double longitud) {
        Double diferenciaLatitudes = Math.toRadians(latitud - cuadrante.getLatitud());
        Double diferenciaLongitudes = Math.toRadians(longitud - cuadrante.getLongitud());
        Double radianesLatitudDeCuadrante = Math.toRadians(cuadrante.getLatitud());
        Double radianesLatitud = Math.toRadians(latitud);

        Double harvesine_parte1 = Math.pow(Math.sin(diferenciaLatitudes / 2),2) + Math.pow(Math.sin(diferenciaLongitudes / 2),2) * Math.cos(radianesLatitudDeCuadrante) * Math.cos(radianesLatitud);
        Double harvesine_parte2 = 2 * Math.asin(Math.sqrt(harvesine_parte1));
        return R * harvesine_parte2;
    }

    public static Boolean coordenadaDentroDelCuadrante(Cuadrante cuadrante, Double latitud, Double longitud){
        Double diferenciaEnLatitud = Math.pow(latitud - cuadrante.getLatitud(),2);
        Double diferenciaEnLongitud = Math.pow(longitud - cuadrante.getLongitud(),2);
        Double radio = cuadrante.calcularRadio2()/100;
        return diferenciaEnLatitud+diferenciaEnLongitud<radio;
    }
}
