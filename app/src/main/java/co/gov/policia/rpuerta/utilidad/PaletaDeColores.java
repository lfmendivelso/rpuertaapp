package co.gov.policia.rpuerta.utilidad;


import android.graphics.Color;

/**
 * Created by lfmendivelso on 27/12/16.
 */

public class PaletaDeColores {

    public static final Integer VERDE_TRANSPARENCIA= Color.argb(75,0,255,0);
    public static final Integer AMARILLO_TRANSPARENCIA= Color.argb(75,255,255,0);
    public static final Integer ROJO_TRANSPARENCIA= Color.argb(75,255,0,0);
    public static final Integer AZUL_TRANSPARENCIA= Color.argb(75,0,0,255);
    public static final Integer MORADO_TRANSPARENCIA= Color.argb(75,255,0,255);
    public static final Integer CYAN_TRANSPARENCIA= Color.argb(75,0,255,255);
    public static final Integer ROJOOSCURO_TRANSPARENCIA= Color.argb(75,59,11,11);
    public static final Integer GRIS_TRANSPARENCIA= Color.argb(75,88,88,88);
    public static final Integer[] COLORES = {VERDE_TRANSPARENCIA,AMARILLO_TRANSPARENCIA,ROJO_TRANSPARENCIA,AZUL_TRANSPARENCIA,MORADO_TRANSPARENCIA,CYAN_TRANSPARENCIA,ROJOOSCURO_TRANSPARENCIA,GRIS_TRANSPARENCIA};

    public static Integer generarColor(Integer rojo,Integer verde,Integer azul){
        return Color.argb(75,rojo,verde,azul);
    }
}
