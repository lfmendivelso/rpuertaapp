package co.gov.policia.rpuerta.utilidad;

import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.gov.policia.rpuerta.modelo.Pregunta;

/**
 * Created by lfmendivelso on 19/12/16.
 */

public class SpinnerUtilidad {

    public static final String SPINNER_ENTRADA = "----------";

    public static ArrayList<String> obtenerListaDeNombres(List lista){
        ArrayList<String> nombres = new ArrayList<>();
        nombres.add(SPINNER_ENTRADA);
        for(int i=0;i<lista.size();i++){
            nombres.add(lista.get(i).toString());
        }
        return nombres;
    }

    public static ArrayList<String> obtenerListaDePreguntas(List<Pregunta> lista){
        ArrayList<String> nombres = new ArrayList<>();
        for(int i=0;i<lista.size();i++){
            nombres.add(lista.get(i).getAyuda());
        }
        return nombres;
    }

    public static Boolean validarSpinner(Spinner spinner){
        Boolean valido = true;
        View itemSeleccionado = spinner.getSelectedView();
        if (itemSeleccionado != null && itemSeleccionado instanceof TextView) {
            TextView textoSeleccionado = (TextView) itemSeleccionado;
            if (textoSeleccionado.getText().toString().equals(SPINNER_ENTRADA)) {
                valido=false;
                textoSeleccionado.setError("Seleccione una opción");
            }
        }
        return valido;
    }



}
